import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:luxemall_app/utils/util.dart';

class IconButtonHeader extends StatelessWidget {
  final String assetSvg;
  final Function onTap;
  final int number;

  IconButtonHeader({
    Key key,
    this.assetSvg = "",
    this.onTap,
    this.number = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    return GestureDetector(
      child: Stack(
        children: [
          Material(
            color: Util.thrid.withOpacity(0.1),
            borderRadius: BorderRadius.circular(100),
            child: InkWell(
                splashColor: Colors.grey[400],
                customBorder: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                ),
                child: Container(
                  height: w * 0.1,
                  width: w * 0.1,
                  padding: EdgeInsets.all(w * 0.01),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: Center(
                    child: SvgPicture.asset(
                      assetSvg,
                      color: Util.primary,
                    ),
                  ),
                ),
                onTap: onTap),
          ),
          (number != 0)
              ? Positioned(
                  top: 0,
                  right: 0,
                  child: Container(
                    height: w * 0.05,
                    width: w * 0.05,
                    decoration: BoxDecoration(
                      color: Color(0xFFFF4848),
                      shape: BoxShape.circle,
                      border: Border.all(width: 1.5, color: Colors.white),
                    ),
                    child: Center(
                      child: AutoSizeText(
                        number.toString(),
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Util.second,
                        ),
                        presetFontSizes: [
                          12,
                          10,
                          9,
                          8,
                        ],
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
      onTap: onTap,
    );
  }
}
