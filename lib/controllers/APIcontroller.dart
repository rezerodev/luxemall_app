import 'package:dio/dio.dart' as d;
import 'package:flutter/services.dart';
import 'package:get/state_manager.dart';
import 'package:intl/intl.dart';
import 'package:luxemall_app/api/api.dart';
import 'package:luxemall_app/models/cart.dart';
import 'package:luxemall_app/models/product.dart' as p;
import 'package:get/get.dart';

class APIController extends GetxController {
  d.Dio dio = d.Dio();
  d.Response response;

  var productsUpdate = <p.Product>[].obs;
  var cartsUpdate = <Cart>[].obs;
  var isGetDataComplete = false.obs;
  var isGetCartComplete = false.obs;
  var isInternetNetworkSuccess = true.obs;
  var stage = 0.obs;
  var categoryProduct = "all".obs;
  var isSortUp = false.obs;
  var singleProduct = p.Product().obs;
  var carts = <p.Product>[].obs;
  var quantityCart = 1.obs;
  var numberCart = 1.obs;
  var numberNotif = 1.obs;
  var totalPrice = 0.0.obs;

  int index = 0;
  List<p.Product> products = [];
  List<int> productsId = [];
  List<int> qualities = [];

  bool streamCondition = true;

  @override
  void onInit() async {
    // TODO: implement onInit
    super.onInit();
    categoryProduct.value = "all";
    stage.value = 0;
    await getDataProductCategorySelected();
    await getDataCart();

    Stream.periodic(const Duration(seconds: 5))
        .takeWhile((_) => streamCondition)
        .forEach((e) {
      if (!isInternetNetworkSuccess.value) {
        categoryProduct.value = "all";
        stage.value = 0;
        getDataProductCategorySelected();
        print("send data");
        Get.snackbar(
            "Information", "Network Error, Check your internet connection");
      }
    });
  }

  @override
  void onClose() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    print("close");
    streamCondition = false;
    super.onClose();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    print("dispose");
    streamCondition = false;
    super.dispose();
  }

  // functions to get data by id
  // ===========================================================================
  Future<void> getProductById(int id) async {
    isGetDataComplete.value = false;
    String uri = API.product;
    try {
      response = await dio.get(uri + id.toString());
      if (response.statusCode == 200) {
        isInternetNetworkSuccess.value = true;
        isGetDataComplete.value = true;

        if (response.data != null) {
          print("respon data => ${response.data}");
          singleProduct.value = p.Product.fromMap(response.data);
        }
      }
    } on d.DioError catch (ex) {
      _dioError(ex);
    }
  }

  // functions to get data according to categories
  // ===========================================================================
  Future<void> getDataProductCategorySelected() async {
    if (categoryProduct.value == "all") {
      getProductLimit4("all", API.productFirstLimit);
    } else if (categoryProduct.value == "electronics") {
      getProductLimit4("electronics", API.categoryElectronicsFirstLimit);
    } else if (categoryProduct.value == "jewelery") {
      getProductLimit4("jewelery", API.categoryJeweleryFirstLimit);
    } else if (categoryProduct.value == "men's clothing") {
      getProductLimit4("men's clothing", API.categoryMenClothingFirstLimit);
    } else if (categoryProduct.value == "women's clothing") {
      getProductLimit4("women's clothing", API.categoryWomenClothingFirstLimit);
    }
  }

  // function for get data
  // ===========================================================================
  getProductLimit4(String category, String api) async {
    isSortUp.value = false;
    switch (stage.value) {
      case 0:
        index = 0;
        products.clear();
        await getProductFirstLimit(api, category);
        if (products.length != 0) {
          index = products[products.length - 1].id;
          stage.value = 1;
        }
        break;
      case 1:
        // print("length => ${products.length}");
        for (int i = 0; i < 4; i++) {
          if (stage.value == 0) {
            break;
          }
          if (products.length == 0) {
            print("nilai i $i");
            stage.value = 0;
            break;
          } else {
            index = products[products.length - 1].id + 1;
            await getProductByIdandCategory(index, category);
          }
        }
        break;
    }
  }

  Future<void> getProductFirstLimit(String uri, String category) async {
    isGetDataComplete.value = false;
    if (category == "all") {
      stage.value = 0;
    }

    try {
      response = await dio.get(uri);
      products.clear();
      if (response.statusCode == 200) {
        isInternetNetworkSuccess.value = true;
        isGetDataComplete.value = true;
        if (category == "all") {
          if (response.data != null || response.data.length != 0) {
            response.data.forEach((item) {
              p.Product tempProduct = p.Product.fromMap(item);
              products.add(tempProduct);
            });

            productsUpdate.assignAll(products);
          }
        } else {
          if (response.data != null || response.data.length != 0) {
            response.data.forEach((item) {
              if (item["category"] == category) {
                p.Product tempProduct = p.Product.fromMap(item);
                products.add(tempProduct);
              }
            });

            productsUpdate.assignAll(products);
          }
        }
      }
    } on d.DioError catch (ex) {
      _dioError(ex);
    }
  }

  Future<void> getProductByIdandCategory(int id, String category) async {
    isGetDataComplete.value = false;
    String uri = API.product;

    try {
      response = await dio.get(uri + id.toString());
      if (response.statusCode == 200) {
        isInternetNetworkSuccess.value = true;
        isGetDataComplete.value = true;
        if (category == "all") {
          if (response.data != null) {
            p.Product tempProduct = p.Product.fromMap(response.data);
            products.add(tempProduct);
            productsUpdate.assignAll(products);
          }
        } else {
          if (response.data != null && response.data["category"] == category) {
            p.Product tempProduct = p.Product.fromMap(response.data);
            products.add(tempProduct);
            productsUpdate.assignAll(products);
          }
        }
        // print("% response => ${response.data}");
        // print("# products length => ${products.length}");
      }
    } on d.DioError catch (ex) {
      _dioError(ex);
    }
  }

  //============================================================================
  //
  // post data to send product_id and quantity to server
  postAddCart() async {
    isGetDataComplete.value = false;
    String uri = API.addToCart;

    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd').format(now);

    Map<String, dynamic> productData = {
      "userId": 10,
      "date": formattedDate,
      "products": [
        {"productId": singleProduct.value.id, "quantity": quantityCart.value},
      ]
    };

    try {
      response = await dio.post(uri, data: productData);
      if (response.statusCode == 200) {
        print("# respon => ${response.data}");
        isInternetNetworkSuccess.value = true;
        isGetDataComplete.value = true;
      }
    } on d.DioError catch (ex) {
      _dioError(ex);
    }
  }

  //============================================================================
  //
  // get cart data
  Future<void> getDataCart() async {
    isGetDataComplete.value = false;
    String uri = API.getDataCartByUser1;
    productsId.clear();
    qualities.clear();

    try {
      response = await dio.get(uri);
      if (response.statusCode == 200) {
        isInternetNetworkSuccess.value = true;
        isGetDataComplete.value = true;

        response.data.forEach((item) {
          Cart cart = Cart.fromMap(item);
          cart.products.forEach((item) {
            productsId.add(item.productId);
            qualities.add(item.quantity);
          });
        });

        numberCart.value = productsId.length;
        // print("productsId => ${productsId}");
        // print("productsId length=> ${productsId.length}");
      }
    } on d.DioError catch (ex) {
      _dioError(ex);
    }
  }

  getEachCart() async {
    int y = 0;
    carts.clear();
    totalPrice.value = 0.0;
    isGetCartComplete.value = false;

    await Future.forEach(productsId, (num) async {
      // print("nilai e $num");
      await getProductById(num).then((v) async {
        carts.add(singleProduct.value);
        // var t = singleProduct.value.price * qualities[y];
        totalPrice.value += (singleProduct.value.price * qualities[y]);
        y++;
      });
    });
    isGetCartComplete.value = true;
  }

  //============================================================================

  //  network error detection
  _dioError(d.DioError ex) {
    if (ex.type == d.DioErrorType.response) {
      //error respon 400,500
      isInternetNetworkSuccess.value = false;
      print("error http respon 400,500");
    } else if (ex.type == d.DioErrorType.other) {
      //example wifi off
      isInternetNetworkSuccess.value = false;
      print("error http default");
    } else {
      //timeout dan cancel
      isInternetNetworkSuccess.value = false;
      print("error time out or cancel");
    }
  }

  //============================================================================
  List<p.Product> temp = [];
  ascDesc() {
    if (isSortUp.value) {
      isSortUp.value = false;
      var tempASC = temp.reversed.toList();
      productsUpdate.assignAll(tempASC);
    } else {
      isSortUp.value = true;

      temp = products.reversed.toList();
      productsUpdate.assignAll(temp);
    }
  }

  Future<void> clearObj() async {
    isGetDataComplete.value = false;
    singleProduct.value.id = 0;
    singleProduct.value.category = "";
    singleProduct.value.description = "";
    singleProduct.value.image = "";
    singleProduct.value.price = 0;
    singleProduct.value.title = "";
  }

  quantityIncrease() {
    quantityCart++;
  }

  quantityDecrease() {
    quantityCart--;
    if (quantityCart.value < 1) {
      quantityCart.value = 1;
    }
  }
}
