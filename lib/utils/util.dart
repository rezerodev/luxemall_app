import 'package:flutter/material.dart';

class Util {
  static Color bgColor = Colors.white;
  static Color primary = Color(0xFFC2912E);
  static Color second = Color(0xFFFFFFFF);
  static Color thrid = Color(0xFF585858);

  static double padding = 20;

  // static String category_1 = "Electronics";
  // static String category_1 = "Electronics";
  // static String category_1 = "Electronics";

  static List<Map<String, dynamic>> category = [
    {"icon": "assets/icons/database.svg", "text": "All"},
    {"icon": "assets/icons/electronics.svg", "text": "Electronics"},
    {"icon": "assets/icons/jewelry.svg", "text": "Jewelery"},
    {"icon": "assets/icons/man_clothes.svg", "text": "Men's clothing"},
    {"icon": "assets/icons/dress.svg", "text": "Women's clothing"},
  ];
}
