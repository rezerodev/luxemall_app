class API {
  static String products = 'https://fakestoreapi.com/products';
  static String product = 'https://fakestoreapi.com/products/';
  static String productFirstLimit = "https://fakestoreapi.com/products?limit=4";
  static String categoryElectronicsFirstLimit =
      "https://fakestoreapi.com/products/category/electronics?limit=4";
  static String categoryJeweleryFirstLimit =
      "https://fakestoreapi.com/products/category/jewelery?limit=4";
  static String categoryMenClothingFirstLimit =
      "https://fakestoreapi.com/products/category/men's clothing?limit=4";
  static String categoryWomenClothingFirstLimit =
      "https://fakestoreapi.com/products/category/women's clothing?limit=4";
  static String addToCart = "https://fakestoreapi.com/carts";
  static String getDataCartByUser1 = "https://fakestoreapi.com/carts/user/1";
}
