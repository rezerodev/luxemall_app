import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:luxemall_app/controllers/APIcontroller.dart';
import 'package:luxemall_app/pages/cart/components/cardCart.dart';
import 'package:luxemall_app/pages/cart/components/checkoutCard.dart';
import 'package:luxemall_app/utils/util.dart';

class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  final APIController apiController = Get.put(APIController());
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 200)).then((onValue) {
      apiController.getEachCart();
    });
  }

  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: buildAppBar(context),
      body: Obx(
        () => apiController.isGetCartComplete.value
            ? Container(
                height: double.maxFinite,
                width: double.maxFinite,
                padding: EdgeInsets.all(Util.padding),
                color: Util.bgColor,
                child: ListView.builder(
                  padding: EdgeInsets.zero,
                  // controller: _scrollController,
                  itemCount: apiController.productsId.length,
                  itemBuilder: (context, index) {
                    return CardCart(
                      height: 100,
                      title: apiController.carts[index].title,
                      price: apiController.carts[index].price.toString(),
                      quantity: apiController.qualities[index],
                    );
                  },
                ),
              )
            : Container(
                height: double.maxFinite,
                width: double.maxFinite,
                child: Stack(
                  children: [
                    SpinKitChasingDots(
                      color: Util.primary,
                      size: h * 0.1,
                    ),
                  ],
                ),
              ),
      ),
      bottomNavigationBar:
          CheckoutCard(totalPrice: apiController.totalPrice.value),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Util.second,
      elevation: 0,
      title: Column(
        children: [
          Text(
            "Your Cart",
            style: TextStyle(color: Colors.black),
          ),
          Text(
            "items",
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios_outlined,
          color: Util.thrid,
        ),
        onPressed: () => Get.back(),
      ),
    );
  }
}
