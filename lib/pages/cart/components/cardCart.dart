import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:luxemall_app/utils/util.dart';

class CardCart extends StatefulWidget {
  final double height;
  final String url;
  final String title;
  final String price;
  final int productId;
  final int quantity;

  const CardCart({
    Key key,
    this.height = 60,
    this.url = "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
    this.productId = 1,
    this.quantity = 1,
    this.title = "",
    this.price = "",
  }) : super(key: key);
  @override
  _CardCartState createState() => _CardCartState();
}

class _CardCartState extends State<CardCart> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Container(
      height: widget.height,
      width: widget.height,
      child: Row(
        children: [
          Container(
            height: widget.height,
            width: widget.height,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(h * 0.025),
                topLeft: Radius.circular(h * 0.025),
              ),
              image: DecorationImage(
                  image: NetworkImage(widget.url), fit: BoxFit.fitHeight),
            ),
          ),
          SizedBox(width: w * 0.01),
          Expanded(
            child: Container(
              child: Column(
                children: [
                  AutoSizeText(
                    widget.title,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Util.thrid,
                    ),
                    textAlign: TextAlign.left,
                    presetFontSizes: [16],
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Row(
                    children: [
                      AutoSizeText(
                        widget.price,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Util.primary,
                        ),
                        textAlign: TextAlign.left,
                        presetFontSizes: [
                          20,
                        ],
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      AutoSizeText(
                        " x${widget.quantity}",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Util.thrid,
                        ),
                        textAlign: TextAlign.left,
                        presetFontSizes: [
                          20,
                        ],
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
