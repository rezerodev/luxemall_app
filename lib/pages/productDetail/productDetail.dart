import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:luxemall_app/controllers/APIcontroller.dart';
import 'package:luxemall_app/pages/productDetail/components/ProductDetailHeader.dart';
import 'package:luxemall_app/pages/productDetail/components/addCartContainer.dart';
import 'package:luxemall_app/utils/util.dart';

import 'components/bottomMenu.dart';

class ProductDetail extends StatefulWidget {
  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  final APIController apiController = Get.put(APIController());

  double h = Get.height;
  double w = Get.width;
  double headerHeight = 80;
  double btmMenuHeight = 50;
  double detailHeight = 30;
  int id = 1;

  @override
  void initState() {
    super.initState();
    id = Get.arguments;

    Future.delayed(Duration(milliseconds: 200)).then((onValue) {
      apiController.getProductById(id);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: Stack(
          children: [
            Container(
              width: double.maxFinite,
              height: double.maxFinite,
              color: Util.second,
              child: Column(
                children: [
                  SafeArea(
                    child: ProductDetailHeader(
                      height: headerHeight,
                      numberCart: apiController.numberCart.value,
                      numberNotif: apiController.numberNotif.value,
                      width: w,
                    ),
                  ),
                  Obx(
                    () => Expanded(
                      child: apiController.isGetDataComplete.value
                          ? Container(
                              height: double.maxFinite,
                              width: double.maxFinite,
                              padding: EdgeInsets.only(bottom: h * 0.01),
                              child: ListView(
                                padding: EdgeInsets.zero,
                                children: [
                                  Container(
                                    height: h * 0.45,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: NetworkImage(apiController
                                              .singleProduct.value.image),
                                          fit: BoxFit.fitHeight),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(Util.padding),
                                    alignment: Alignment.topLeft,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        AutoSizeText(
                                          apiController
                                              .singleProduct.value.price
                                              .toString(),
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            color: Util.thrid,
                                          ),
                                          textAlign: TextAlign.left,
                                          presetFontSizes: [
                                            30,
                                            25,
                                            20,
                                          ],
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        AutoSizeText(
                                          apiController
                                              .singleProduct.value.title,
                                          style: TextStyle(
                                            color: Util.thrid,
                                            fontWeight: FontWeight.w600,
                                          ),
                                          textAlign: TextAlign.justify,
                                          presetFontSizes: [16, 15, 14],
                                          maxLines: 3,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        SizedBox(height: h * 0.015),
                                        AutoSizeText(
                                          "Description : ",
                                          style: TextStyle(
                                            color: Util.thrid,
                                          ),
                                          textAlign: TextAlign.justify,
                                          presetFontSizes: [15, 14, 13, 12],
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        SizedBox(height: h * 0.006),
                                        AutoSizeText(
                                          apiController
                                              .singleProduct.value.description,
                                          style: TextStyle(
                                            color: Util.thrid,
                                          ),
                                          textAlign: TextAlign.justify,
                                          presetFontSizes: [14, 13, 12],
                                          maxLines: 20,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Container(
                              height: double.maxFinite,
                              width: double.maxFinite,
                            ),
                    ),
                  ),
                  BottomMenu(
                    btmMenuHeight: btmMenuHeight,
                    onAddCartTap: () {
                      if (apiController.isGetDataComplete.value) {
                        apiController.quantityCart.value = 1;
                        Get.bottomSheet(
                          AddCartContainer(
                            btnHeight: btmMenuHeight,
                            heightContainer: h * 0.4,
                            image: apiController.singleProduct.value.image,
                            price: apiController.singleProduct.value.price
                                .toString(),
                            onAddCartTap: () async {
                              await apiController.postAddCart();
                              Get.back();
                            },
                          ),
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
            Obx(
              () => apiController.isGetDataComplete.value
                  ? Container()
                  : Container(
                      height: double.maxFinite,
                      width: double.maxFinite,
                      alignment: Alignment.center,
                      child: SpinKitChasingDots(
                        color: Util.primary,
                        size: h * 0.1,
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
