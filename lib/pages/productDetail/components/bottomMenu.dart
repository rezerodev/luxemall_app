import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:luxemall_app/pages/productDetail/components/CustomButton.dart';
import 'package:luxemall_app/utils/util.dart';

class BottomMenu extends StatefulWidget {
  final double btmMenuHeight;
  final Function onAddCartTap;

  const BottomMenu({
    Key key,
    this.btmMenuHeight,
    this.onAddCartTap,
  }) : super(key: key);

  @override
  _BottomMenuState createState() => _BottomMenuState();
}

class _BottomMenuState extends State<BottomMenu> {
  double w;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    w = MediaQuery.of(context).size.width;
    return Container(
      width: double.maxFinite,
      height: widget.btmMenuHeight,
      decoration: BoxDecoration(
        color: Util.second,
        border: Border(
          top: BorderSide(width: 0.2, color: Util.thrid),
        ),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: w * 0.025),
        width: w,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              alignment: Alignment.center,
              width: 40,
              color: Util.second,
              child: IconButton(
                icon: SvgPicture.asset("assets/icons/Chat bubble Icon.svg",
                    color: Util.thrid),
                onPressed: () {},
              ),
            ),
            SizedBox(width: w * 0.025),
            Expanded(
              child: CustomButton(
                bgColor: Util.primary,
                height: widget.btmMenuHeight * 0.75,
                child: AutoSizeText(
                  "Add to Cart",
                  style: TextStyle(
                    color: Util.second,
                    fontWeight: FontWeight.w600,
                  ),
                  textAlign: TextAlign.center,
                  presetFontSizes: [15, 14, 13, 12],
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                onTap: widget.onAddCartTap,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
