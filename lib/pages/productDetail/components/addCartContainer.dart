import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:luxemall_app/controllers/APIcontroller.dart';
import 'package:luxemall_app/pages/productDetail/components/CustomButton.dart';
import 'package:luxemall_app/pages/productDetail/components/CustomIconButton.dart';
import 'package:luxemall_app/utils/util.dart';

class AddCartContainer extends StatelessWidget {
  final APIController apiController = Get.put(APIController());
  final String image;
  final String price;
  final double heightContainer;
  final double btnHeight;
  final Function onAddCartTap;

  AddCartContainer({
    Key key,
    this.image = "",
    this.price = "",
    this.heightContainer = 250,
    this.btnHeight = 45,
    this.onAddCartTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Container(
      height: heightContainer,
      width: double.maxFinite,
      padding: EdgeInsets.all(Util.padding),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(h * 0.01),
          topRight: Radius.circular(h * 0.01),
        ),
        color: Util.second,
      ),
      child: Column(
        children: [
          Expanded(
              flex: 4,
              child: Row(
                children: [
                  Container(
                    height: double.maxFinite,
                    width: w * 0.3,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(image), fit: BoxFit.fitHeight),
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Container(
                      width: double.maxFinite,
                      height: double.maxFinite,
                      alignment: Alignment.topLeft,
                      child: AutoSizeText(
                        price,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Util.thrid,
                        ),
                        textAlign: TextAlign.left,
                        presetFontSizes: [
                          30,
                          25,
                          20,
                        ],
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ],
              )),
          Divider(
            thickness: 0.1,
            color: Util.thrid,
          ),
          Expanded(
            flex: 2,
            child: Container(
              child: Row(
                children: [
                  Expanded(
                    child: AutoSizeText(
                      "Quantity",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Util.thrid,
                      ),
                      textAlign: TextAlign.left,
                      presetFontSizes: [
                        20,
                      ],
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Container(
                    height: double.maxFinite,
                    width: w * 0.4,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        CustomIconButton(
                          width: w * 0.1,
                          height: w * 0.1,
                          bgColor: Util.primary,
                          child: Icon(
                            Icons.add,
                            color: Util.second,
                          ),
                          onTap: () {
                            apiController.quantityIncrease();
                          },
                        ),
                        Expanded(
                          child: Obx(
                            () => Container(
                              padding:
                                  EdgeInsets.symmetric(horizontal: w * 0.01),
                              child: AutoSizeText(
                                apiController.quantityCart.value.toString(),
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Util.thrid,
                                ),
                                textAlign: TextAlign.center,
                                presetFontSizes: [25, 22, 20, 18, 16],
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ),
                        CustomIconButton(
                          width: w * 0.1,
                          height: w * 0.1,
                          bgColor: Util.second,
                          borderColor: Util.thrid,
                          child: Icon(
                            Icons.remove,
                            color: Util.thrid,
                          ),
                          onTap: () {
                            apiController.quantityDecrease();
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              height: double.maxFinite,
              width: double.maxFinite,
              alignment: Alignment.bottomCenter,
              child: CustomButton(
                bgColor: Util.primary,
                height: btnHeight,
                width: w,
                radius: w * 0.015,
                child: AutoSizeText(
                  "Add to Cart",
                  style: TextStyle(
                    color: Util.second,
                    fontWeight: FontWeight.w600,
                  ),
                  textAlign: TextAlign.center,
                  presetFontSizes: [15, 14, 13, 12],
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                onTap: onAddCartTap,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
