import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:luxemall_app/components/iconButtonHeader.dart';
import 'package:luxemall_app/pages/cart/cart.dart';
import 'package:luxemall_app/pages/productDetail/components/productDetailSearch.dart';

class ProductDetailHeader extends StatelessWidget {
  final double height;
  final double width;
  final int numberCart;
  final int numberNotif;

  const ProductDetailHeader({
    Key key,
    this.height,
    this.width,
    this.numberCart = 0,
    this.numberNotif = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: double.maxFinite,
      padding: EdgeInsets.fromLTRB(0, 20, 10, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            onPressed: () {
              Get.back();
            },
          ),
          ProductDetailSearch(
            height: 45,
            width: MediaQuery.of(context).size.width * 0.6,
          ),
          IconButtonHeader(
            assetSvg: "assets/icons/Cart Icon.svg",
            number: numberCart,
            onTap: () {
              Get.to(() => Cart());
            },
          ),
          IconButtonHeader(
            assetSvg: "assets/icons/Bell.svg",
            number: numberNotif,
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
