import 'package:flutter/material.dart';

class CustomIconButton extends StatelessWidget {
  final double height;
  final double width;
  final Function onTap;
  final Widget child;
  final Color bgColor;
  final Color borderColor;
  final double radius;

  const CustomIconButton({
    Key key,
    this.height = 40,
    this.width = 40,
    this.onTap,
    this.bgColor = Colors.blue,
    this.child,
    this.borderColor = Colors.transparent,
    this.radius = 100,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: bgColor,
      borderRadius: BorderRadius.circular(radius),
      child: InkWell(
          splashColor: Colors.grey[400],
          customBorder: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius),
          ),
          child: Container(
            height: height,
            width: width,
            padding: EdgeInsets.all(height * 0.1),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(radius),
                border: Border.all(width: 1, color: borderColor)),
            child: Center(child: child),
          ),
          onTap: onTap),
    );
  }
}