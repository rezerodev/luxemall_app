import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:luxemall_app/pages/home/home.dart';
import 'package:luxemall_app/utils/util.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 2)).then((onValue) {
      Get.off(() => Home());
    });
  }

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        alignment: Alignment.center,
        color: Util.primary,
        child: Text(
          "LUXEMALL",
          style: TextStyle(
            color: Util.second,
            fontSize: w * 0.1,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
