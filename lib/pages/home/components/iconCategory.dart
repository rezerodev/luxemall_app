import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:luxemall_app/utils/util.dart';

class IconCategory extends StatelessWidget {
  final double height;
  final double width;
  final String label;
  final String assetIcon;
  final Function onTap;

  const IconCategory({
    Key key,
    this.height,
    this.width,
    this.label = "",
    this.assetIcon = "",
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // double h = MediaQuery.of(context).size.height;
    return Container(
      height: height,
      width: width,
      child: Column(
        children: [
          Material(
            color: Util.primary,
            borderRadius: BorderRadius.circular(height * 0.1),
            child: InkWell(
                splashColor: Colors.grey[400],
                customBorder: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(height * 0.1),
                ),
                child: Container(
                  height: height * 0.57,
                  width: height * 0.57,
                  padding: EdgeInsets.all(height * 0.1),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(height * 0.1),
                  ),
                  child: SvgPicture.asset(
                    assetIcon,
                    color: Util.second,
                  ),
                ),
                onTap: onTap),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(top: height * 0.05),
              alignment: Alignment.center,
              child: AutoSizeText(
                label,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: Util.primary,
                ),
                textAlign: TextAlign.center,
                presetFontSizes: [
                  12,
                  11,
                  10,
                  9,
                  8,
                ],
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
