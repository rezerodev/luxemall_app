import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:luxemall_app/controllers/APIcontroller.dart';
import 'package:luxemall_app/pages/cart/cart.dart';
import 'package:luxemall_app/pages/home/components/homeSearch.dart';
import 'package:luxemall_app/components/iconButtonHeader.dart';

class HomeHeader extends StatelessWidget {
  final APIController apiController = Get.put(APIController());
  final double height;

  HomeHeader({
    Key key,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: double.maxFinite,
      padding: EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          HomeSearch(
            height: 45,
            width: MediaQuery.of(context).size.width * 0.6,
          ),
          Obx(
            () => IconButtonHeader(
              assetSvg: "assets/icons/Cart Icon.svg",
              number: apiController.numberCart.value,
              onTap: () {
                print("cart");
                Get.to(() => Cart());
              },
            ),
          ),
          Obx(
            () => IconButtonHeader(
              assetSvg: "assets/icons/Bell.svg",
              number: apiController.numberNotif.value,
              onTap: () {
                print("notification");
              },
            ),
          ),
        ],
      ),
    );
  }
}
