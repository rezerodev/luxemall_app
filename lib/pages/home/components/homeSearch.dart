import 'package:flutter/material.dart';
import 'package:luxemall_app/utils/util.dart';

class HomeSearch extends StatelessWidget {
  final double height;
  final double width;
  final TextEditingController controller;
  final Function onTap;
  final Function(String) onChanged;
  final Function(String) onSubmitted;

  const HomeSearch({
    Key key,
    this.height,
    this.width,
    this.controller,
    this.onTap,
    this.onChanged,
    this.onSubmitted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      // height: height,
      alignment: Alignment.center,
      // padding: EdgeInsets.symmetric(vertical: 4),
      decoration: BoxDecoration(
        color: Util.thrid.withOpacity(0.1),
        borderRadius: BorderRadius.circular(15),
      ),
      child: TextField(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 5,
          ),
          prefixIcon: Icon(
            Icons.search,
            color: Util.thrid,
          ),
          border: InputBorder.none,
          enabledBorder: InputBorder.none,
          hintText: "Search Product",
        ),
        textInputAction: TextInputAction.search,
        controller: controller,
        onTap: onTap,
        onChanged: onChanged,
        onSubmitted: onSubmitted,
      ),
    );
  }
}
