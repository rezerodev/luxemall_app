import 'package:flutter/material.dart';
import 'package:luxemall_app/utils/util.dart';

class ProductBox extends StatelessWidget {
  final double height;
  final double width;
  final double heightScreen;
  final String url;
  final String title;
  final String price;
  final Function onTap;

  ProductBox({
    Key key,
    this.height,
    this.width,
    this.heightScreen = 250,
    this.url = "",
    this.title = "",
    this.price = "",
    this.onTap,
  }) : super(key: key);

  double paddingButtom;
  double boxHeight;

  @override
  Widget build(BuildContext context) {
    paddingButtom = heightScreen * 0.025;
    boxHeight = (heightScreen - paddingButtom - (2 * Util.padding)) * 0.5;

    return GestureDetector(
      child: Container(
        height: boxHeight,
        width: double.maxFinite,
        margin: EdgeInsets.only(bottom: paddingButtom),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(heightScreen * 0.025),
          border: Border.all(
            color: Util.thrid,
            width: 0.5,
          ),
        ),
        child: Column(
          children: [
            Container(
              height: boxHeight * 0.7,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(heightScreen * 0.025),
                  topLeft: Radius.circular(heightScreen * 0.025),
                ),
                image: DecorationImage(
                    image: NetworkImage(url), fit: BoxFit.fitHeight),
              ),
            ),
            Expanded(
              child: Container(
                height: double.maxFinite,
                width: double.maxFinite,
                padding: EdgeInsets.fromLTRB(
                  heightScreen * 0.025,
                  heightScreen * 0.025,
                  heightScreen * 0.025,
                  heightScreen * 0.01,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(heightScreen * 0.025),
                    bottomRight: Radius.circular(heightScreen * 0.025),
                  ),
                ),
                child: Column(
                  children: [
                    Expanded(
                      flex: 4,
                      child: Container(
                        height: double.maxFinite,
                        width: double.maxFinite,
                        alignment: Alignment.center,
                        child: Text(
                          title,
                          style: TextStyle(
                            color: Util.thrid,
                            fontSize: heightScreen * 0.03,
                            fontWeight: FontWeight.w600,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        height: double.maxFinite,
                        width: double.maxFinite,
                        alignment: Alignment.center,
                        // color: Colors.green,
                        child: Text(
                          price,
                          style: TextStyle(
                            color: Util.thrid,
                            fontSize: heightScreen * 0.03,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      onTap: onTap,
    );
  }
}
