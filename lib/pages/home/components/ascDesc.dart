import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:luxemall_app/utils/util.dart';

class AscDesc extends StatefulWidget {
  final double height;
  final double width;
  final String assetIcon;
  final Function onTap;

  const AscDesc({
    Key key,
    this.height,
    this.width,
    this.assetIcon = "",
    this.onTap,
  }) : super(key: key);

  @override
  _AscDescState createState() => _AscDescState();
}

class _AscDescState extends State<AscDesc> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(horizontal: Util.padding),
      alignment: Alignment.centerRight,
      child: Material(
        color: Util.second,
        borderRadius: BorderRadius.circular(widget.height * 0.5),
        child: InkWell(
            splashColor: Colors.grey[400],
            customBorder: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(widget.height * 0.5),
            ),
            child: Container(
              height: widget.height * 0.65,
              width: widget.width * 0.2,
              padding: EdgeInsets.all(widget.height * 0.1),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(widget.height * 0.5),
                  border: Border.all(width: 0.5, color: Util.thrid)),
              child: SvgPicture.asset(
                widget.assetIcon,
                color: Util.primary,
              ),
            ),
            onTap: widget.onTap),
      ),
    );
  }
}
