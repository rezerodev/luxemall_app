import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:luxemall_app/pages/home/home.dart';
import 'package:luxemall_app/utils/util.dart';

class CustomBottomNavigation extends StatefulWidget {
  final double btnNavHeight;

  const CustomBottomNavigation({Key key, this.btnNavHeight}) : super(key: key);
  @override
  _CustomBottomNavigationState createState() => _CustomBottomNavigationState();
}

class _CustomBottomNavigationState extends State<CustomBottomNavigation> {
  List<bool> isBottomIconTap = [false, false, false, false];

  @override
  void initState() {
    super.initState();
    _onItemTapped(0);
  }

  void _onItemTapped(int index) {
    setState(() {
      isBottomIconTap = [false, false, false, false];

      isBottomIconTap[index] = true;

      if (isBottomIconTap[0]) {
        print("go to home");
        Get.to(() => Home());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: widget.btnNavHeight,
      decoration: BoxDecoration(
        color: Util.second,
        border: Border(
          top: BorderSide(width: 0.2, color: Util.thrid),
        ),
      ),
      child: Center(
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                color: Util.second,
                child: IconButton(
                  icon: SvgPicture.asset("assets/icons/Shop Icon.svg",
                      color: isBottomIconTap[0] ? Util.primary : Util.thrid),
                  onPressed: () {
                    _onItemTapped(0);
                  },
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                color: Util.second,
                child: IconButton(
                  icon: SvgPicture.asset("assets/icons/Heart Icon.svg",
                      color: isBottomIconTap[1] ? Util.primary : Util.thrid),
                  onPressed: () {
                    _onItemTapped(1);
                  },
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                color: Util.second,
                child: IconButton(
                  icon: SvgPicture.asset("assets/icons/Chat bubble Icon.svg",
                      color: isBottomIconTap[2] ? Util.primary : Util.thrid),
                  onPressed: () {
                    _onItemTapped(2);
                  },
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                color: Util.second,
                child: IconButton(
                  icon: SvgPicture.asset("assets/icons/User Icon.svg",
                      color: isBottomIconTap[3] ? Util.primary : Util.thrid),
                  onPressed: () {
                    _onItemTapped(3);
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
