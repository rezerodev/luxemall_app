import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:luxemall_app/controllers/APIcontroller.dart';
import 'package:luxemall_app/pages/home/components/iconCategory.dart';
import 'package:luxemall_app/utils/util.dart';

class Category extends StatelessWidget {
  // final APIController apiController = Get.put(APIController());
  final double height;
  final double width;
  final Function onTapAll;
  final Function onTapElec;
  final Function onTapJewel;
  final Function onTapMen;
  final Function onTapWomen;

  Category({
    Key key,
    this.height,
    this.width,
    this.onTapAll,
    this.onTapElec,
    this.onTapJewel,
    this.onTapMen,
    this.onTapWomen,
  }) : super(key: key);

  double boxWidth;

  @override
  Widget build(BuildContext context) {
    boxWidth = width * 0.2;
    return Container(
      height: height,
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(horizontal: Util.padding),
      child: ListView(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        scrollDirection: Axis.horizontal,
        children: [
          IconCategory(
            height: boxWidth,
            width: boxWidth,
            label: Util.category[0]["text"],
            assetIcon: Util.category[0]["icon"],
            onTap: onTapAll,
          ),
          IconCategory(
            height: boxWidth,
            width: boxWidth,
            label: Util.category[1]["text"],
            assetIcon: Util.category[1]["icon"],
            onTap: onTapElec,
          ),
          IconCategory(
            height: boxWidth,
            width: boxWidth,
            label: Util.category[2]["text"],
            assetIcon: Util.category[2]["icon"],
            onTap: onTapJewel,
          ),
          IconCategory(
            height: boxWidth,
            width: boxWidth,
            label: Util.category[3]["text"],
            assetIcon: Util.category[3]["icon"],
            onTap: onTapMen,
          ),
          IconCategory(
            height: boxWidth,
            width: boxWidth,
            label: Util.category[4]["text"],
            assetIcon: Util.category[4]["icon"],
            onTap: onTapWomen,
          ),
        ],
      ),
    );
  }
}
