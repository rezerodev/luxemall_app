import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:luxemall_app/controllers/APIcontroller.dart';
import 'package:luxemall_app/pages/home/components/ascDesc.dart';
import 'package:luxemall_app/pages/home/components/category.dart';
import 'package:luxemall_app/pages/home/components/productBox.dart';
import 'package:luxemall_app/pages/productDetail/productDetail.dart';
import 'package:luxemall_app/utils/util.dart';

class Product extends StatefulWidget {
  final double heightCalculate;
  final double heightCategory;
  final double width;

  const Product({
    Key key,
    this.heightCalculate = 250.0,
    this.heightCategory = 60,
    this.width,
  }) : super(key: key);

  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  final APIController apiController = Get.put(APIController());

  ScrollController _scrollController = new ScrollController();

  getDataAtCategory(String category) async {
    apiController.stage.value = 0;
    apiController.categoryProduct.value = category;
    await apiController.getDataProductCategorySelected();
    _scrollController.animateTo(
      0.0,
      curve: Curves.easeOut,
      duration: const Duration(milliseconds: 300),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.maxFinite,
      width: double.maxFinite,
      child: Column(
        children: [
          Category(
            height: widget.heightCategory,
            width: widget.width,
            onTapAll: () async {
              await getDataAtCategory("all");
            },
            onTapElec: () async {
              await getDataAtCategory("electronics");
            },
            onTapJewel: () async {
              await getDataAtCategory("jewelery");
            },
            onTapMen: () async {
              await getDataAtCategory("men's clothing");
            },
            onTapWomen: () async {
              await getDataAtCategory("women's clothing");
            },
          ),
          Obx(
            () => AscDesc(
              height: 40,
              width: widget.width,
              assetIcon: apiController.isSortUp.value
                  ? "assets/icons/descending.svg"
                  : "assets/icons/ascending.svg",
              onTap: () async {
                if (apiController.isGetDataComplete.value) {
                  _scrollController.animateTo(
                    0.0,
                    curve: Curves.easeOut,
                    duration: const Duration(milliseconds: 300),
                  );
                  apiController.ascDesc();
                }
              },
            ),
          ),
          Expanded(
            child: NotificationListener<ScrollEndNotification>(
              onNotification: (scrollEnd) {
                var metrics = scrollEnd.metrics;
                if (metrics.atEdge) {
                  if (metrics.pixels == 0)
                    print('At top');
                  else {
                    print('At bottom');
                    apiController.getDataProductCategorySelected();
                  }
                }
                return true;
              },
              child: Obx(
                () => ListView.builder(
                  padding: EdgeInsets.fromLTRB(
                      Util.padding, 0, Util.padding, Util.padding),
                  controller: _scrollController,
                  itemCount: apiController.productsUpdate.length,
                  itemBuilder: (context, index) {
                    return ProductBox(
                      heightScreen: widget.heightCalculate,
                      url: apiController.productsUpdate[index].image,
                      title: apiController.productsUpdate[index].title,
                      price:
                          apiController.productsUpdate[index].price.toString(),
                      onTap: () async {
                        apiController.clearObj();
                        Get.to(() => ProductDetail(),
                            arguments: apiController.productsUpdate[index].id);
                      },
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
