import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:luxemall_app/controllers/APIcontroller.dart';
import 'package:luxemall_app/pages/home/components/customBottomNavigation.dart';
import 'package:luxemall_app/pages/home/components/homeHeader.dart';
import 'package:luxemall_app/pages/home/components/product.dart';
import 'package:luxemall_app/utils/util.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final APIController apiController = Get.put(APIController());

  final GlobalKey _key = GlobalKey();

  double h = Get.height;
  double w = Get.width;
  double btnNavHeight = 50;
  double headerHeight = 80;
  double categoryHeight;
  double heightCalculate;
  double paddingofTopScreen;
  double offsetCalculateScreen = 34;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => getProductSize());
    print("# first limit");
  }

  screenCalculate(BuildContext context) {
    categoryHeight = h * 0.11;
    paddingofTopScreen = MediaQuery.of(context).padding.top;
  }

  getProductSize() {
    RenderBox box = _key.currentContext.findRenderObject();
    Size size = box.size;
    setState(() {
      heightCalculate = size.height;
    });
    print("pCalulate = ${size.height}");
  }

  @override
  Widget build(BuildContext context) {
    screenCalculate(context);
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Util.second,
        child: Column(
          children: [
            SafeArea(
              child: HomeHeader(
                height: headerHeight,
              ),
            ),
            Expanded(
              child: Obx(() => Container(
                      child: Stack(
                    children: [
                      Product(
                        heightCalculate: heightCalculate,
                        heightCategory: categoryHeight,
                        width: w,
                        key: _key,
                      ),
                      apiController.isGetDataComplete.value
                          ? Container()
                          : SpinKitChasingDots(
                              color: Util.primary,
                              size: h * 0.1,
                            ),
                    ],
                  ))),
            ),
            CustomBottomNavigation(
              btnNavHeight: btnNavHeight,
            )
          ],
        ),
      ),
    );
  }
}
