// To parse this JSON data, do
//
//     final cart = cartFromMap(jsonString);

import 'dart:convert';

Cart cartFromMap(String str) => Cart.fromMap(json.decode(str));

String cartToMap(Cart data) => json.encode(data.toMap());

class Cart {
  Cart({
    this.id,
    this.userId,
    this.date,
    this.products,
    this.v,
  });

  int id;
  int userId;
  DateTime date;
  List<Product> products;
  int v;

  factory Cart.fromMap(Map<String, dynamic> json) => Cart(
        id: json["id"] == null ? null : json["id"],
        userId: json["userId"] == null ? null : json["userId"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        products: json["products"] == null
            ? null
            : List<Product>.from(
                json["products"].map((x) => Product.fromMap(x))),
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "userId": userId == null ? null : userId,
        "date": date == null ? null : date.toIso8601String(),
        "products": products == null
            ? null
            : List<dynamic>.from(products.map((x) => x.toMap())),
        "__v": v == null ? null : v,
      };
}

class Product {
  Product({
    this.productId,
    this.quantity,
  });

  int productId;
  int quantity;

  factory Product.fromMap(Map<String, dynamic> json) => Product(
        productId: json["productId"] == null ? null : json["productId"],
        quantity: json["quantity"] == null ? null : json["quantity"],
      );

  Map<String, dynamic> toMap() => {
        "productId": productId == null ? null : productId,
        "quantity": quantity == null ? null : quantity,
      };
}
